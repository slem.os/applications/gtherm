Name:       gtherm
Version:    0.0.2
Release:    1%{?dist}
Summary:    Thermal information for GNOME

License:        GPLv3+ and MIT
URL:            https://source.puri.sm/Librem5/calls
%undefine _disable_source_fetch
Source0:        https://source.puri.sm/Librem5/gtherm/-/archive/v%{version}/%{name}-v%{version}.tar.gz

BuildRequires:  gcc
BuildRequires:  meson
BuildRequires:  cmake

BuildRequires:  gcc-c++
BuildRequires:  pkgconfig(gobject-2.0)
BuildRequires:  pkgconfig(glib-2.0) >= 2.50.0
BuildRequires:  pkgconfig(gtk+-3.0)
BuildRequires:  pkgconfig(libhandy-0.0) >= 0.0.12
BuildRequires:  pkgconfig(gsound)
BuildRequires:  pkgconfig(libpeas-1.0)
BuildRequires:  pkgconfig(gom-1.0)
BuildRequires:  pkgconfig(libebook-contacts-1.2)
BuildRequires:  pkgconfig(folks)
BuildRequires:  pkgconfig(mm-glib)
BuildRequires:  appstream-glib 
BuildRequires:  vala
BuildRequires:  desktop-file-utils
BuildRequires:  xvfb-run
BuildRequires:  xauth
BuildRequires:  libappstream-glib8
BuildRequires:  feedbackd-devel

Requires: hicolor-icon-theme


%description
gtherm provides a DBus daemon (gthd) to monitor thermal zones and cooling
devices. It offers a library (libgtherm) and GObject introspection bindings to
ease reading the values in applications.

%package devel
Summary: Development files for %{name}
Requires: %{name}%{?_isa} = %{version}-%{release}

%description devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.


%prep
%setup -q -n %{name}-v%{version}

%build
%meson
%meson_build


%install
%meson_install


%files
%doc README.md
%license COPYING
%{_bindir}/gthcli
%{_bindir}/gthd
%dir %{_libdir}/girepository-1.0
%{_libdir}/girepository-1.0/GTherm-0.0.typelib
%{_libdir}/libgtherm-0.0.so.0
%dir %{_libdir}/pkgconfig/
%{_libdir}/pkgconfig/libgtherm-0.0.pc
%dir %{_datadir}/dbus-1/interfaces/
%{_datadir}/dbus-1/interfaces/org.sigxcpu.Thermal.CoolingDevice.xml
%{_datadir}/dbus-1/interfaces/org.sigxcpu.Thermal.ThermalZone.xml

%files devel
%defattr(-,root,root,-)
%{_libdir}/libgtherm-0.0.so
%dir %{_includedir}/libgtherm-0.0
%{_includedir}/libgtherm-0.0/gth-cooling-device.h
%{_includedir}/libgtherm-0.0/gth-gdbus.h
%{_includedir}/libgtherm-0.0/gth-manager.h
%{_includedir}/libgtherm-0.0/gth-names.h
%{_includedir}/libgtherm-0.0/gth-thermal-zone.h
%{_includedir}/libgtherm-0.0/libgtherm.h
%{_datadir}/gir-1.0/GTherm-0.0.gir
%dir %{_datadir}/vala/vapi/
%{_datadir}/vala/vapi/libgtherm-0.0.deps
%{_datadir}/vala/vapi/libgtherm-0.0.vapi

%changelog
* Sun Jul 26 2020 Adrian Campos Garrido <adriancampos@teachelp.com> - 0.0.2-1
- First build


